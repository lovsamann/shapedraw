//
//  ViewController.m
//  ShapeDraw
//
//  Created by Ann on 11/17/15.
//  Copyright © 2015 Ann. All rights reserved.
//

#import "ViewController.h"

#import "ShapeView.h"

@interface ViewController ()<ShapeViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet ShapeView *shapeView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.shapeView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Disposឪf any resources that can be recreated.
}

#pragma mark - override delegate method

-
(void)didChangeShape:(ShapeView *)shapeView withType:(ShapeType)type {
    NSString *text = nil;
    
    if (type == Circle) {
        text = @"Circle";
    }else if (type == Triangle) {
        text = @"Triangle";
    }else if (type == Rectangle) {
        text = @"Rectangle";
    }else{
        text = @"Pentagon";
    }
    
    self.descriptionLabel.text = [NSString stringWithFormat:@"View has been changed to %@",text];
}

@end
