//
//  ShapeView.m
//  ShapeDraw
//
//  Created by Ann on 11/17/15.
//  Copyright © 2015 Ann. All rights reserved.
//

#import "ShapeView.h"

#define COLOR_VALUE (arc4random() % 100) / 100.0

@implementation ShapeView


#pragma mark - initial

-(void)awakeFromNib {
    [self setUp];
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setUp];
    }
    return self;
}

-(void)setShapeType:(ShapeType)shapeType{
    _shapeType = shapeType;
    [self setNeedsDisplay];
}

#pragma mark - private methods


-(void)changeShape{
    //avoid deplicate views
    NSMutableArray *array = [NSMutableArray arrayWithObjects:@0, @1, @2, @3, nil];
    for (int i = 0; i < [array count]; i++) {
        if (self.shapeType == [[array objectAtIndex:i] intValue]) {
            [array removeObjectAtIndex:i];
            break;
        }
    }
    
    
    int index = arc4random() % [array count];
    self.shapeType = [array[index]intValue];
    [self setNeedsDisplay];
}

-(void)setUp{
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
    [self addGestureRecognizer:tap];
}

-(void)drawShape:(CGContextRef)context forShape:(ShapeType) type {
    
    if (self.shapeType == Circle) {
        [self drawCircle:context];
    }else if (self.shapeType == Pentagon) {
        [self drawPentagon:context];
    }else if (self.shapeType == Triangle) {
        [self drawTriangle:context];
    }else{
        [self drawRectangle:context];
    }
}

#pragma mark - override method

-(void)drawRect:(CGRect)rect {
    //random color
    [[UIColor colorWithRed:COLOR_VALUE green:COLOR_VALUE blue:COLOR_VALUE alpha:1.0] setFill];
    //create context
    CGContextRef context = UIGraphicsGetCurrentContext();
    //draw shape
    [self drawShape:context forShape:self.shapeType];
    
}

#pragma mark - handle events

-(void)tapped:(UITapGestureRecognizer *) sender{

    [self changeShape];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didChangeShape:withType:)]) {
        [self.delegate didChangeShape:self withType:self.shapeType];
    }
    

}

#pragma mark - draw shape

- (void)drawCircle:(CGContextRef)context {
    UIGraphicsPushContext(context);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
    [path addClip];
    [path fill];
    
    UIGraphicsPopContext();
}

- (void)drawTriangle:(CGContextRef)context {
    UIGraphicsPushContext(context);
    UIBezierPath *path=[UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(45, 0)];
    [path addLineToPoint:CGPointMake(90, 90)];
    [path addLineToPoint:CGPointMake(0, 90)];

    
    [path closePath];
    [path fill];
}

- (void)drawRectangle:(CGContextRef)context {
    UIGraphicsPushContext(context);
    
    CGContextAddRect(context, self.bounds);

    CGContextFillPath(context);
    
    UIGraphicsPopContext();
}

-(void)drawPentagon:(CGContextRef)context {
    UIGraphicsPushContext(context);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    // Set the starting point of the shape.
    [path moveToPoint:CGPointMake(45.0, 0.0)];
    
    // Draw the lines.
    [path addLineToPoint:CGPointMake(90.0, 20.0)];
    [path addLineToPoint:CGPointMake(80, 70)];
    [path addLineToPoint:CGPointMake(20.0, 70)];
    [path addLineToPoint:CGPointMake(0.0, 20.0)];
    [path closePath];
    
    
    [UIColor colorWithRed:0.1 green:1 blue:1 alpha:1];
    [path fill];
}

@end
