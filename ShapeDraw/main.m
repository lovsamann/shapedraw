//
//  main.m
//  ShapeDraw
//
//  Created by Ann on 11/17/15.
//  Copyright © 2015 Ann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
