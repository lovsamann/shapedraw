//
//  ShapeView.h
//  ShapeDraw
//
//  Created by Ann on 11/17/15.
//  Copyright © 2015 Ann. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ShapeType) {
    Circle,
    Triangle,
    Rectangle,
    Pentagon
};

@class ShapeView;

@protocol ShapeViewDelegate<NSObject>

@optional
-(void)didChangeShape:(ShapeView *)shapeView withType:(ShapeType) type;

@end


@interface ShapeView : UIView

@property(nonatomic) ShapeType shapeType;
@property(nonatomic,weak) id<ShapeViewDelegate> delegate;

@end
